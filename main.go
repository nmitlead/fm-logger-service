package main

import (
	"logger/conf"
	"logger/event"

	"fmt"
)

var (
	cf *conf.AppConf
)

func Init() error {
	var err error

	cf, err = conf.GetConf()
	if err != nil {
		return fmt.Errorf("get conf error: %s", err)
	}

	if err := event.Init(); err != nil {
		return fmt.Errorf("event Init error: %s", err)
	}

	return nil
}

func main() {
	if err := Init(); err != nil {
		fmt.Println("Init error: ", err)
		return
	}

	forever := make(chan bool)

	if err := event.ListenLogs(); err != nil {
		fmt.Println("Listen logs error: ", err)
		return
	}

	<-forever
}
