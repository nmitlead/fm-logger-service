package conf

import (
	"testing"
)

func TestGetConf(t *testing.T) {
	if _, err := GetConf(); err != nil {
		t.Error("GetConf() error: ", err)
	}
}
