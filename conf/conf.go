package conf

import (
	"fm-libs/config"
)

var (
	ConfDir = "conf"
	RunMode = "dev"
	appConf *AppConf
)

type AppConf struct {
	Rmq   config.Rmq
	Redis config.Redis

	RoutingKeys struct {
		All string
	}

	Addr string

	Version string
	Deps    map[string]string
}

func (c *AppConf) SetDefaults() {
	c.RoutingKeys.All = "#"
	c.Redis.SetDefaults()
	c.Rmq.Url = "amqp://localhost:5672"

	c.Addr = ":3300"

	c.Version = "1.0.0"
	c.Deps = map[string]string{
		"fm-libs": "0.4.0",
	}
}

func GetConf() (*AppConf, error) {
	var err error

	//appConf not initialized initialize it
	if appConf == nil {
		cf := AppConf{}

		cf.SetDefaults()

		confFile := ConfDir + "/app." + RunMode + ".toml"
		if err = config.ReadResources(&cf, confFile, "env"); err != nil {
			return nil, err
		}

		appConf = &cf
	}

	return appConf, nil
}
