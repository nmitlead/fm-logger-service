package models

import (
	"bytes"
	"net/http"
	"time"
)

var date_layout = "2006-01-02"

type Log struct {
}

func (l Log) Log(service_name string, data []byte) (*http.Response, error) {
	return http.Post(
		"http://localhost:9200/log-"+string(time.Now().Format(date_layout))+"/"+service_name+"/",
		"application/json",
		bytes.NewBuffer(data),
	)
}
