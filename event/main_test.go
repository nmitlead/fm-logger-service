package event

import (
	"fm-libs/log"
	"logger/conf"

	"os"
	"testing"
)

const (
	ACL_ROUTE_KEY = "acl.fleet.created"
)

var (
	schema string
)

func TestMain(m *testing.M) {
	conf.ConfDir = "../conf"
	conf.RunMode = "test"

	_, err := conf.GetConf()
	if err != nil {
		log.Log("get conf", log.FatalLevel, log.M{
			"error": err.Error(),
		})
	}

	if err := Init(); err != nil {
		log.Log("init ", log.FatalLevel, log.M{
			"error": err.Error(),
		})
	}

	exitCode := m.Run()

	os.Exit(exitCode)
}
