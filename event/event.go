package event

import (
	"github.com/streadway/amqp"

	rbt "fm-libs/rabbit"
	"logger/conf"
	"logger/models"
	"logger/rabbit"

	"fmt"
	"strings"
)

var (
	rmq *amqp.Connection
	cf  *conf.AppConf
)

const (
	ListenAllQueue = "logger"
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: %s", err)
	}

	return nil
}

//Init event module
func Init() error {
	var err error
	if err := initConf(); err != nil {
		return err
	}

	if rmq, err = rabbit.GetRabbit(); err != nil {
		return err
	}

	return nil
}

//Listen for fleet create event and create new schema and insert initial data
//returns fleetId on success and errors on error
func ListenLogs() error {
	options := make(map[string]bool)
	queue := ListenAllQueue

	if conf.RunMode == "test" {
		queue = ""
		options["queue_auto_delete"] = true
	}

	_, err := rbt.Watch(rmq, queue, cf.RoutingKeys.All, rbt.ExchangeLogs, func(event amqp.Delivery) {
		log := models.Log{}

		routing_key := strings.Split(event.RoutingKey, ".")
		service_name := routing_key[0]

		_, err := log.Log(service_name, event.Body)
		if err != nil {
			fmt.Println("post error: ", err)
			return
		}

	}, options)

	return err
}
